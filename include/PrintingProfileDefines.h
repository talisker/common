// Copyright 2017, 2018, 2019 InnoVisioNate Inc. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#pragma once

#define OUTLINES_PAGE_RECORD_SIZE 34
#define OUTLINES_PAGE_RECORD_FORMAT  "%05s%04ld:%04ld-%04ld%06ld-%08ld"

#define OUTLINES_ENTRY_RECORD_PREAMBLE_SIZE  24
#define OUTLINES_ENTRY_RECORD_PREAMBLE_FORMAT "%04ld,%04ld,%04ld,%04ld:%04ld"