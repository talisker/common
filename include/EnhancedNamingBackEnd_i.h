

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 8.01.0622 */
/* at Mon Jan 18 22:14:07 2038
 */
/* Compiler settings for EnhancedNamingBackEnd.odl:
    Oicf, W1, Zp8, env=Win64 (32b run), target_arch=AMD64 8.01.0622 
    protocol : all , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */



/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 500
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif /* __RPCNDR_H_VERSION__ */


#ifndef __EnhancedNamingBackEnd_i_h__
#define __EnhancedNamingBackEnd_i_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __CursiVisionNamingBackEnd_FWD_DEFINED__
#define __CursiVisionNamingBackEnd_FWD_DEFINED__

#ifdef __cplusplus
typedef class CursiVisionNamingBackEnd CursiVisionNamingBackEnd;
#else
typedef struct CursiVisionNamingBackEnd CursiVisionNamingBackEnd;
#endif /* __cplusplus */

#endif 	/* __CursiVisionNamingBackEnd_FWD_DEFINED__ */


/* header files for imported files */
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 



#ifndef __CursiVisionNamingBackEnd_LIBRARY_DEFINED__
#define __CursiVisionNamingBackEnd_LIBRARY_DEFINED__

/* library CursiVisionNamingBackEnd */
/* [version][lcid][helpstring][uuid] */ 


DEFINE_GUID(LIBID_CursiVisionNamingBackEnd,0x2BEFEC20,0xB6F7,0x4ccb,0x93,0xB7,0xD8,0xED,0xDF,0x31,0x53,0xB5);

DEFINE_GUID(CLSID_CursiVisionNamingBackEnd,0x2BEFEC20,0xB6F7,0x4ccb,0x93,0xB7,0xD8,0xED,0xDF,0x31,0x53,0xB6);

#ifdef __cplusplus

class DECLSPEC_UUID("2BEFEC20-B6F7-4ccb-93B7-D8EDDF3153B6")
CursiVisionNamingBackEnd;
#endif
#endif /* __CursiVisionNamingBackEnd_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


