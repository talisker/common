

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 8.00.0603 */
/* at Fri Mar 09 13:34:11 2018
 */
/* Compiler settings for PlotTypes.odl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 8.00.0603 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__


#ifndef __PlotTypes_i_h__
#define __PlotTypes_i_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __PlotTypes_FWD_DEFINED__
#define __PlotTypes_FWD_DEFINED__

#ifdef __cplusplus
typedef class PlotTypes PlotTypes;
#else
typedef struct PlotTypes PlotTypes;
#endif /* __cplusplus */

#endif 	/* __PlotTypes_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 



#ifndef __PlotTypes_LIBRARY_DEFINED__
#define __PlotTypes_LIBRARY_DEFINED__

/* library PlotTypes */
/* [version][lcid][helpstring][uuid] */ 


EXTERN_C const IID LIBID_PlotTypes;

EXTERN_C const CLSID CLSID_PlotTypes;

#ifdef __cplusplus

class DECLSPEC_UUID("8CBEFD7F-55E6-11d3-8365-006008BD5BC3")
PlotTypes;
#endif
#endif /* __PlotTypes_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


